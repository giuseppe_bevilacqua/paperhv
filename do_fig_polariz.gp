# aggiusto lo spessore degli assi
#set border lw 2

## per i font 
set loadpath "/usr/share/texmf/tex/latex/base/"

# epslatex
set terminal epslatex size 3.5,2.62 standalone color colortext 12
set output 'fig_polariz.tex'

### process with
### latex fig_global_eigen.tex
### dvips -E fig_global_eigen.dvi -o fig_global_eigen.eps

# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2  # blue
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2  # red

# Legend
#set key at 6.1,1.3
unset key

set border 0

unset ytics
unset xtics

# Axes label 
#set xlabel '$t/w$'
#set ylabel 'Eigenvalues'

## aggiusto l'asse y

f(x) = (1 + tanh(200*cos(x)))/2.0

set samples 500

set label '$\epsilon_x$' at -10, 4.5
set label '$\epsilon_y$' at -10, 2.5
set label '$\epsilon_x^2 - \epsilon_y^2$' at -11.5,0

set label '1' at 10.5, 5
set label '0' at 10.5, 4

set label '1' at 10.5, 3
set label '0' at 10.5, 2

set label '1' at 10.5, 1
set label '-1' at 10.5, -1


plot f(x) +4 lt -1 , f(x+pi)+2 lt -1, f(x) - f(x+pi) lt -1

