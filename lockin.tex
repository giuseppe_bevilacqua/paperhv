\documentclass[pra,a4paper]{revtex4-1}
%\documentclass[aps, showpacs, pra,twocolumn, a4paper]{revtex4-1}
%\usepackage{psfrag}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{ulem}
\usepackage{bbm}


\newcommand{\TreJ}[6]{
  \begin{pmatrix}
    #1 & #2 & #3 \\
    #4 & #5 & #6
  \end{pmatrix}
}

\newcommand{\SeiJ}[6]{
  \left\{
    \begin{matrix}
      #1 & #2 & #3 \\
      #4 & #5 & #6
    \end{matrix}
  \right\}
}

\newcommand{\roij}[2]{\phantom{}_{#1 #2}\rho}
\newcommand{\roijkq}[4]{\phantom{}_{#1 #2}\rho^{#3}_{#4}}
\newcommand{\ropertijkq}[5]{\phantom{}^{#1}_{#2 #3}\rho^{#4}_{#5}}
\newcommand{\rodotijkq}[4]{\phantom{}_{#1 #2}\dot{\rho}^{#3}_{#4}}
\newcommand{\Tijkq}[4]{\phantom{}_{#1 #2}T^{#3}_{#4}}
%
\newcommand{\ket}[1]{\mbox{$\left| #1 \right\rangle $}}
\newcommand{\bra}[1]{\mbox{$\left\langle #1 \right| $}}
\newcommand{\ps}[2]{\mbox{$\left\langle #1 |#2 \right\rangle $}}

%
\newcommand{\Real}{\mathop{\mathrm{Re\,}}}
\newcommand{\Imag}{\mathop{\mathrm{Im\,}}}

\DeclareMathOperator{\e}{\displaystyle e}


\begin{document}

\title{Lock-in with an arbitrary phase }
%
%
\author{G.  Bevilacqua}
\affiliation{Department    of   Information
  Engineering and Mathematical Science,  University of Siena, Via Roma
  56, 53100 Siena, Italy}
\email[]{giuseppe.bevilacqua@unisi.it}
%%
\author{E. Breschi}
%
\affiliation{Department of Physics, University of Fribourg, Chemin
du Mus\'{e}e 3, 1700 Fribourg, Switzerland}
%
%
%
\begin{abstract}
How I believe the lock-in works
\end{abstract}

\date{\today}

\pacs{
  32.60.+i, % Zeeman and Stark effects
  32.70.Jz, % Line shapes, widths, and shifts
  33.40.+f, % Multiple resonances
  32.80.Xx, % Optical pumping of atoms,
}


\maketitle


\section{Lock-in}

The signal to be analyzed is written as 
\begin{equation}
  \label{eq:kappa:fin}
\kappa(t) = c_0 + \sum_{n \neq 0} c_n \, \e^{i\, n \, \Omega\, t}
= c_0 + \sum_{n>0}\,\bigg[ \Re( c_n  + c_{-n}) \cos( n\Omega\,t) - \Im( c_n -
c_{-n}) \sin( n\Omega\,t) \bigg]
\end{equation}
where the coefficients satisfy 
\begin{equation}
  \label{eq:prop:cn}
  c_{-n} = c_n^*
\end{equation}
because $\kappa(t)$ is
a real quantity. 

Before entering the lock-in is multiplied by a reference signal
\begin{equation}
  \label{eq:refe}
  r(t) = A \cos( k\Omega t + \varphi) = \frac{A}{2}\left( 
    \e^{i  \, k\,\Omega\,t + i\varphi} + \e^{-i  \, k\,\Omega\,t - i\varphi} \right)
\end{equation}
The lock-in action is that of an RC-circuit
\begin{equation}
  \label{eq:lock-in}
  \kappa_{out}(t)     =    \frac{1}{\tau}\int_0^t    \e^{-(t-t')/\tau}
  \kappa(t') r(t') d\,t'
\end{equation}
where $\tau$ is the time constant of the RC.

The integral is elementary and in the long time limit ($t\gg\tau$) the
result is
\begin{equation}
  \label{eq:res:lock}
  \kappa_{out}(t) = \frac{A}{2}\sum_n c_n
  \left\{ 
    \frac{\e^{i(n+k)\Omega\,t} \, \e^{i\,\varphi}}{ 1 + i(n+k)\Omega\tau} +
    \frac{\e^{i(n-k)\Omega\,t} \, \e^{-i\,\varphi}}{ 1 + i(n-k)\Omega\tau}
  \right\}
\end{equation}
If the  $\tau$ is chosen so  that $\Omega\tau \gg1$  the only resonant
contribution  are $n=-k$  in the  first sum  and $n=k$  in  the second
sum. So that 
\begin{equation}
  \label{eq:res:contr:lock}
  \kappa_{out}(t)  = \frac{A}{2}\big[  (c_k  + c_{-k})\,  \cos \varphi  -
  i(c_k - c_{-k}) \, \sin \varphi \big]
\end{equation}
and using (\ref{eq:prop:cn}) finally
\begin{equation}
  \label{eq:fin:lock:contr}
  \kappa_{out}(t) = A\bigg\{ \big[ \Re(c_k + c_{-k})\big] 
    \, \cos \varphi 
    - \big[ - \Im(c_k - c_{-k}) \big] \, \sin \varphi \bigg\} 
\end{equation}

That formula shows that for  $\varphi=0$, i.e. demodulating with a simple
cosinus $\cos (k\Omega t) $ one obtains the so called
``in-phase''  signal.   The   $\varphi=\pi/2$  case  correspond  to
demodulation  with  a  simple  sinus  $\sin(k\Omega  t)$  getting  the
``in-quadrature'' signal. 

If the phase $\varphi$ is a free parameter then 
\begin{align}
  \label{eq:final:phi:gen}
  \kappa_{out}^{phase} & =  \big[ \Re(c_k + c_{-k})\big] 
  \, \cos \varphi 
  -  \big[ -  \Im(c_k  - c_{-k})  \big]  \, \sin  \varphi \qquad
  \mathrm{reference} 
\;\;
\cos(k\Omega t + \varphi) \\
  % 
  \kappa_{out}^{quadr} & = \big[ \Re(c_k + c_{-k})\big] 
  \, \sin \varphi 
  + \big[ - \Im(c_k - c_{-k}) \big] \, \cos \varphi
\qquad 
\mathrm{reference}
\;\;
\sin(k\Omega t + \varphi) 
\end{align}

Regarding the hv paper the above formulas give
\begin{itemize}
\item odd harmonics
\begin{align}
    \mathrm{phase} &= \bigg\{ 
    \big[ M_1^{(I)}\, \mathcal{L}_{-2\omega_L} + M_1^{(R)}\, \mathcal{D}_{-2\omega_L}
    \big] + 
    % 
    \big[ - M_1^{(I)}\, \mathcal{L}_{2\omega_L} + M_1^{(R)}\, \mathcal{D}_{2\omega_L}
    \big] + \\
    % 
    & \big[ M_2^{(I)}\, \mathcal{L}_{-\omega_L} + M_2^{(R)}\, \mathcal{D}_{-\omega_L}
    \big] + 
    % 
    \big[ -M_2^{(I)}\, \mathcal{L}_{\omega_L} + M_2^{(R)}\, \mathcal{D}_{\omega_L}
    \big] + \\
    % 
    & M_3 \, \mathcal{D}_0 \bigg\}\, \cos\varphi + \\
%%%%
%%
    & \bigg\{ 
%
    \big[ M_1^{(R)}\, \mathcal{L}_{2\omega_L} + M_1^{(I)}\, 
    \mathcal{D}_{2\omega_L} \big] + 
    % 
    \big[  M_1^{(R)}\, \mathcal{L}_{-2\omega_L} - M_1^{(I)}\, \mathcal{D}_{-2\omega_L}
    \big] + \\
  %
    & 
    \big[ M_2^{(R)}\, \mathcal{L}_{\omega_L} + M_2^{(I)}\, 
    \mathcal{D}_{\omega_L} \big] + 
    % 
    \big[  M_2^{(R)}\, \mathcal{L}_{-\omega_L} - M_2^{(I)}\, \mathcal{D}_{-\omega_L}
    \big] + \\
    % 
    & M_3 \, \mathcal{L}_0 
%
\bigg\} \sin\varphi
  \end{align}
  which shows that, for instance, choosing 
  $\varphi = \arctan(M_1^{(R)}/M_1^{(I)} )$
 the  dispersive  contribution  to  the resonance  at  $2\omega_L$  is
 zeroed. 

It follows also
\begin{align}
    \mathrm{quadrature} &= \bigg\{ 
    \big[ M_1^{(I)}\, \mathcal{L}_{-2\omega_L} + M_1^{(R)}\, \mathcal{D}_{-2\omega_L}
    \big] + 
    % 
    \big[ - M_1^{(I)}\, \mathcal{L}_{2\omega_L} + M_1^{(R)}\, \mathcal{D}_{2\omega_L}
    \big] + \\
    % 
    & \big[ M_2^{(I)}\, \mathcal{L}_{-\omega_L} + M_2^{(R)}\, \mathcal{D}_{-\omega_L}
    \big] + 
    % 
    \big[ -M_2^{(I)}\, \mathcal{L}_{\omega_L} + M_2^{(R)}\, \mathcal{D}_{\omega_L}
    \big] + \\
    % 
    & M_3 \, \mathcal{D}_0 \bigg\}\, \sin\varphi - \\
%%%%
%%
    & \bigg\{ 
%
    \big[ M_1^{(R)}\, \mathcal{L}_{2\omega_L} + M_1^{(I)}\, 
    \mathcal{D}_{2\omega_L} \big] + 
    % 
    \big[  M_1^{(R)}\, \mathcal{L}_{-2\omega_L} - M_1^{(I)}\, \mathcal{D}_{-2\omega_L}
    \big] + \\
  %
    & 
    \big[ M_2^{(R)}\, \mathcal{L}_{\omega_L} + M_2^{(I)}\, 
    \mathcal{D}_{\omega_L} \big] + 
    % 
    \big[  M_2^{(R)}\, \mathcal{L}_{-\omega_L} - M_2^{(I)}\, \mathcal{D}_{-\omega_L}
    \big] + \\
    % 
    & M_3 \, \mathcal{L}_0 
%
\bigg\} \cos\varphi
  \end{align}

\item even harmonics

Maybe we do not need this explicitly

\end{itemize}


\end{document}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
